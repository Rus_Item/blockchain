﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlockC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockC.Tests
{
    [TestClass()]
    public class ChainTests
    {
        [TestMethod()]
        public void ChainTest()
        {
            var chain = new Chain();
            chain.Add("Admin", "Admin");

            Assert.AreEqual("Admin", chain.Last.Login);
        }

        [TestMethod()]
        public void CheckTest()
        {
            var chain = new Chain();
            chain.Add("First user", "Admin");
            chain.Add("Rus", "P@ssword");

            Assert.IsTrue(chain.Check());
        }
    }
}