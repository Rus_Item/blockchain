﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlockC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockC.Tests
{
    [TestClass()]
    public class BlockTests
    {
       [TestMethod()]
       public void SerializeTest()
        {
            var block = new Block();
            var json = "{\"Created\":\"\\/Date(1573381800000)\\/\",\"Hash\":\"8165ff242b6b644c77ec88758da7cb5a8a3681bdcb5c8e44da51c71d30db4bfe\",\"Login\":\"First user\",\"Password\":\"Admin\",\"PreviousHash\":\"Password\"}";    
            var resultString = block.Serialize();

            Assert.AreEqual(json, resultString);

            var resultBlock = Block.DeSerialize(resultString);

            Assert.AreEqual(block.Hash, resultBlock.Hash);
            Assert.AreEqual(block.Created, resultBlock.Created);
            Assert.AreEqual(block.Login, resultBlock.Login);
            Assert.AreEqual(block.PreviousHash, resultBlock.PreviousHash);
            Assert.AreEqual(block.Password, resultBlock.Password);
        }
    }
}