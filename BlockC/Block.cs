﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;

namespace BlockC
{
    [DataContract]
    public class Block
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        [DataMember]
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; private set; }

        [DataMember]
        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public string Password { get; private set; }

        [DataMember]
        /// <summary>
        /// Дата и время создания
        /// </summary>
        public DateTime Created { get; private set; }

        [DataMember]
        /// <summary>
        /// Хэш блока
        /// </summary>
        public string Hash { get; private set; }

        [DataMember]
        /// <summary>
        /// Хэш предыдущего блока
        /// </summary>
        public string PreviousHash { get; private set; }

        /// <summary>
        /// Конструктор генезис блока
        /// </summary>
        public Block()
        {
            Id = 1;
            Login = "First user";
            Password = "Admin";
            Created = DateTime.Parse("11.10.2019 16:30:00.000").ToUniversalTime();
            PreviousHash = "Password";

            var data = GetData();
            Hash = GetHash(data);
        }
        
        /// <summary>
        /// Конструктор блока 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="password"></param>
        /// <param name="block"></param>
        public Block(string login, string password, Block block)
        {
            if (string.IsNullOrWhiteSpace(login))
                throw new ArgumentNullException("Пустой аргумент login", nameof(login));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException("Пустой аргумент password", nameof(password));
            if (block == null)
                throw new ArgumentNullException("Пустой аргумент block", nameof(block));
            Login = login;
            Password = GetHash(password);
            Created = DateTime.UtcNow;
            PreviousHash = block.Hash;
            Id = block.Id + 1;
            var blockdata = GetData();
            Hash = GetHash(blockdata);
        }
       
        /// <summary>
        /// Получение значимых данных
        /// </summary>
        /// <returns></returns>
        private string GetData()
        {
            string result = "";

            result += Login;
            result += Password;
            result += Created.ToString("dd.mm.yyyy HH:mm.ss.fff");
            result += PreviousHash;
            return result;
        }

        /// <summary>
        /// Хэширование данных
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string GetHash(string data)
        {
            var message = Encoding.ASCII.GetBytes(data);
            var hashString = new SHA256Managed();
            string hex = "";

            var hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
                hex += String.Format("{0:x2}", x);
            return hex;
        }

        public override string ToString()
        {
            return Login;
        }

        /// <summary>
        /// Выполнить сериализацию объекта в JSON строку
        /// </summary>
        /// <returns></returns>
        public string Serialize()
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(Block));
            using (var ms = new MemoryStream())
            {
                jsonSerializer.WriteObject(ms, this);
                var result = Encoding.UTF8.GetString(ms.ToArray());
                return result;
            }
        }
       
        /// <summary>
        /// Выполнить десериализацию объекта Block из JSON строки
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static Block DeSerialize(string json)
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(Block));
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                var result = (Block)jsonSerializer.ReadObject(ms);
                return result;
            }
        }
    }
}
