﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BlockC
{
    /// <summary>
    /// Цепочка блоков
    /// </summary>
    public class Chain
    {
        /// <summary>
        /// Все блоки
        /// </summary>
        public List<Block> Blocks { get; private set; }
      
        /// <summary>
        /// Последний блок
        /// </summary>
        public Block Last { get; private set; }
        
        /// <summary>
        /// Создание блоков
        /// </summary>
        public Chain()
        {
            Blocks = new List<Block>();
            var genesisBlock = new Block();
            Blocks.Add(genesisBlock);
            Last = genesisBlock;
        }
       
        /// <summary>
        /// Добавление блоков
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public void Add(string login, string password)
        {
            var block = new Block(login, password, Last);
            Blocks.Add(block);
            Last = block;
            Save(Last);
        }
       
        /// <summary>
        /// Добавление цепочки из текстового файла
        /// </summary>
        public void AddFromFile()
        {
            Blocks = LoadChainFromFile();
            if (Blocks.Count == 0)
            {
                var genesisBlock = new Block();
                Blocks.Add(genesisBlock);
                Last = genesisBlock;
                Save(Last);
            }
            else
            {
                if (Check())
                {
                    Last = Blocks.Last();
                }
                else
                {
                    throw new Exception("Ошибка получения блоков из текстового файла. Цепочка не прошла проверку на целостность!");
                }
            }
        }
       
        /// <summary>
        /// Проверка цепочки
        /// </summary>
        /// <returns></returns>
        public bool Check()
        {
            var genesisBlock = new Block();
            var previousHash = genesisBlock.Hash;

            foreach (var block in Blocks.Skip(1))
            {
                var hash = block.PreviousHash;
                if (previousHash != hash)
                    return false;
                previousHash = block.Hash;
            }
            return true;
        }
       
        /// <summary>
        /// Метод записи блока в текстовый файл
        /// </summary>
        /// <param name="block"> Сохраненный блок. </param>
        private void Save(Block block)
        {
            var fileStream = new FileStream(@"file.txt", FileMode.Append, FileAccess.Write);
            using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
            {
                var resultString = block.Serialize();
                streamWriter.WriteLine(resultString);
                streamWriter.Close();
            }
            fileStream.Close();
        }
        
        /// <summary>
        /// Получение данных из текстового файла в цепочку.
        /// </summary>
        /// <returns> Список блоков данных </returns>
        private List<Block> LoadChainFromFile()
        {
            List<Block> result;
            int count = 100;
            result = new List<Block>(count * 2);
            var block = new Block();
            var fileStream = new FileStream(@"file.txt", FileMode.Open, FileAccess.Read);
            if (fileStream.Length != 0)
            {
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    string line;
                    do
                    {
                        line = streamReader.ReadLine();
                        var resultBlock = Block.DeSerialize(line);
                        result.Add(resultBlock);
                    } while (!streamReader.EndOfStream);
                    streamReader.Close();
                }
            }
            fileStream.Close();
            return result;
        }

        private void Sync()
        {
            
        }
    }
}
